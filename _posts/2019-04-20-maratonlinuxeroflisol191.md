---
layout: post
title: "#01 Maratón Linuxero FLISol 2019"
date: 2019-04-20
category: Podcast
youtube:
  id: ITMkIeoAKsw
  start:
    h: 0
    m: 0
    s: 0
podcast:
  audio: https://archive.org/download/MLflisol191/MLflisol191
tags: [audio, podcast, directo]
---
Iniciamos esta edición de Maratón Linuxero haciendo eco del FLISol 2019, evento que tendrá lugar en una semana.
Para nuestra primera sección tuvimos representantes de los países de Alemania, Argentina, Colombia y México.
Alex Bejarano, Diego Accorinti, Fina Porras y Enrique de Fes compartieron las iniciativas que se realizan desde sus sedes y naciones.

Aquí tienes la emisión en YouTube:

{% include youtubePlayer.html %}

Y aquí el audio extraído:

{% include audioPlayer.html %}

Toda la música utilizada en Maratón linuxero tiene licencia Creative Commons:  
 
1. [Way to Success](https://www.jamendo.com/track/1334807/way-to-success) por [Addict Sound](https://www.jamendo.com/artist/451073/addict-sound)


Si quieres **contactar** con nosotros, puedes hacerlo de las siguientes formas:
